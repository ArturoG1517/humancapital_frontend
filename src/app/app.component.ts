import { Component } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title_big = 'HUMAN CAPITAL';

  constructor(private spinner: NgxSpinnerService, private titleService:Title) {
    this.titleService.setTitle("Human Capital");
  }

  ngOnInit() {
    /** spinner starts on init */
    this.spinner.show();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }
}
